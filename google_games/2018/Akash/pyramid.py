# -*- coding: utf-8 -*-
"""
Created on Sun Apr  8 13:48:32 2018

@author: akash
"""

import itertools




lll = [1,2,3,4,5,6,7,8,10,11,12,13,14,15]

def list_lex(N, K):
    def list_lex_helper(n, k, min_val):
        if k == 1:
            for i in range(1, n + 1):
                yield str(i + min_val)
            
            raise StopIteration()

        else:
            for i in range(1, n - k + 2):
                for j in list_lex_helper(n - i, k - 1, min_val + i):
                    yield str(i + min_val) +"," + str(j)
    ret = []
    for i in list_lex_helper(N, K, 0):
        ret.append(i)
    return ret


l = list_lex(14, 5)
#print(l)
l2 = []
for i in l:    
    i = i.strip().split(",")
    temp = []
    for j in i:
        j = int(j)
        if(j>8):
            temp.append(int(j)+1)
        else:
            temp.append(int(j))
    l2.append(list(itertools.combinations(temp,5)))


print(l2)
print("===========")
possibleLists = []
for i in l2:
    s = lll.copy()
    for j in i:
        print(j)
        #s.remove(j)
    possible = 1
    i2 = []
    for x in range(0,4):
        if abs(i[x]-i[x+1]) in s:
            s.remove(abs(i[x]-i[x+1]))
            i2.append(abs(i[x]-i[x+1]))
        else: 
            possible = 0
            break
    if(possible):
        print(str(i) + " " + str(i2))
        i3 = []
        for x in range(0,3):
            if abs(i2[x]-i2[x+1]) in s:
                s.remove(abs(i2[x]-i2[x+1]))
                i3.append(abs(i2[x]-i2[x+1]))
            else: 
                possible = 0
                break
        if(possible):
            print(i3)
        
    



