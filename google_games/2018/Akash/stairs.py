# -*- coding: utf-8 -*-
"""
Created on Sun Apr  8 13:01:48 2018

@author: akash
"""

stuff = open("input.txt",'r')

inputs = []
for x in stuff:
    x = x.strip().split(' ')
    inputs.append(x)
    


del inputs[0]
del inputs[0]

def validXs(x):
    if(x == 0):
        return (0,1)
    if(x==9):
        return(8,9)
    return(x-1,x,x+1)


def stairs(x,y,jumpUsed,money):
    if(jumpUsed):
        xs = validXs(x)
        #print(xs)
        tries = []
        #print(str(x) + " " + str(y))
        if(y < 100):
            for a in xs:
                tries.append(stairs(a,y+1,1,money+int(inputs[y][x])))
        else:
            #print("end")
            return 0
        return max(tries)
    
stairs(0,0,1,0)
    
    


