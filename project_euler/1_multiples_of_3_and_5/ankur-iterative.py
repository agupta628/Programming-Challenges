upper_bound = 1000

sum_ = 0
for i in range(upper_bound):
    if (i % 3 == 0) or (i % 5 == 0):
        sum_ += i

print(sum_)
