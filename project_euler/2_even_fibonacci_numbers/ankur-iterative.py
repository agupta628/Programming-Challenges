upper_bound = 4000000

sum_ = 0
a = 0
b = 1
c = a + b

while c <= upper_bound:
    if c % 2 == 0:
        sum_ += c

    a, b = b, c
    c = a + b

print(sum_)
