# Programming Challenges
Our solutions for different online challenges like Google Games, and more. Also included
are utility functions to help speed up development in the future.

## Challenges Included
In this repo, there are solutions for the following challenges
* Google Games 2018
* Project Euler

# Note to Collaborators
Make sure that **whenever** you create a new file, function, etc., that you **document** it
in the respective `README.md` file. Make sure to include the problem in the README, and
leave execution instructions if it isn't blatantly obvious.
